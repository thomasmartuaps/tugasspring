
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Product List</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<h1 class="display-4">Product List</h1>
<table width="80%" class="table table-striped table-bordered">
<thead>
	<tr>
		<th scope="col">Code</th>
		<th scope="col">Name</th>
		<th scope="col">Type</th>
		<th scope="col">Action</th>
	</tr>
</thead>
<tbody>
	<c:forEach items="${products}" var="product">
	<tr>
	<td><c:out value="${product.code}"></c:out></td>
	<td><c:out value="${product.name}"></c:out></td>
	<td><c:out value="${product.type}"></c:out></td>
	<td><a href="product/edit/${product.id}">View</a></td>
	</tr></c:forEach>
</tbody>
</table>
</body>
</html>