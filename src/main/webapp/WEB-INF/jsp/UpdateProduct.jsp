<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Item</title>
</head>
<body>
<h1>Update Product</h1>
	<form:form method="post" action="/SpringTugas/product/update">
		<h5>Product Name:</h5>
		<br>
		<form:input path="name" type="text" />
		<br>
		<h5>Product Code:</h5>
		<br>
		<form:input path="code" type="text" />
		<br>
		<h5>Product Type:</h5>
		<br>
		<form:input path="type" type="text" />
		<br>
		<h5>Price:</h5>
		<br>
		<form:input path="price" type="number" />
		<br>
		<h5>Stock:</h5>
		<br>
		<form:input path="stock" type="number" />
		<br>
		<form:input path="id" type="hidden" />
		<input type="submit" value="Save" />
	</form:form>
</body>
</html>