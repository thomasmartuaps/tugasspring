<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New Product</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<div class="row">
			<h1 class="display-4">New Product</h1>
		</div>
		<div class="row">
			<form:form method="post" action="create">
				<div class="form-group">
					<label for="name">Product Name</label>

					<form:input path="name" type="text" />
				</div>
				<div class="form-group">
					<label for="code">Product Code</label>
					<form:input path="code" type="text" />
				</div>

				<div class="form-group">
					<label for="type">Product Type</label>
					<form:input path="type" type="text" placeholder="e.g. drinks" />

				</div>

				<div class="form-group">
					<label for="price">Price</label>
					<form:input path="price" type="number" />
				</div>


				<div class="form-group">
					<label for="stock">Stock</label>
					<form:input path="stock" type="number" />
				</div>

				<input class="btn btn-success" type="submit" value="Save" />
			</form:form>
		</div>
	</div>
</body>
</html>