package com.tugas.spring.controller;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.tugas.spring.services.ProductService;
import com.tugas.spring.domain.Product;

@Controller
public class ProductController {
	private ProductService productService = new ProductService();
	
	@GetMapping("/product")
	public String showProductList(Model m) {
//		ProductService productService = new ProductService();
		try {
			List<Product> products = productService.findProducts(0);
			m.addAttribute("products", products);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ProductList";
	}
	
	@GetMapping("/product/new")
	public String showNewProductForm(Model m) {
		m.addAttribute("command", new Product());  
		return "CreateProduct";
	}
	
	@GetMapping("/product/edit/{id}")
	public String showUpdateProductForm(@PathVariable("id") long id, Model m) {
		try {
			List<Product> products = productService.findProducts(id);
			m.addAttribute("command", products.get(0));  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "UpdateProduct";
	}
	
	@PostMapping("/product/create")
	public String create(@ModelAttribute("product") Product newProduct, Model m) {
		System.out.println(newProduct);
		try {
			productService.save(newProduct);
			m.addAttribute("success_type", "added!");
			return "SuccessCreate";
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "create");
			m.addAttribute("message", "Class Not Found Exception");
			return "FailedCreate";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "create");
			m.addAttribute("message", "SQL Error!");
			return "FailedCreate";
		}
	}
	
	@PostMapping("/product/update")
	public String update(@ModelAttribute("product") Product product, Model m) {
		System.out.println(product);
		try {
			productService.update(product, product.getId());
			m.addAttribute("success_type", "updated!");
			return "SuccessCreate";
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "update");
			m.addAttribute("message", "Class Not Found Exception");
			return "FailedCreate";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			m.addAttribute("fail_type", "update");
			m.addAttribute("message", "SQL Error!");
			return "FailedCreate";
		}
	}
}
